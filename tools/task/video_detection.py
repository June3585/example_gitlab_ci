# Copyright (c) OpenMMLab. All rights reserved.
import argparse
import os

import cv2
from mmdeploy_runtime import Detector


def parse_args():
    parser = argparse.ArgumentParser(
        description='show how to use sdk python api')
    parser.add_argument('device_name', help='name of device, cuda or cpu')
    parser.add_argument(
        'model_path',
        help='path of mmdeploy SDK model dumped by model converter')
    parser.add_argument('video_path', help='path of an image')
    args = parser.parse_args()
    return args


def count_video_frames(video_path):
    video_capture = cv2.VideoCapture(video_path)
    if not video_capture.isOpened():
        print('failed to load video')
        exit(-1)

    count = 0
    while True:
        ret, _ = cap.read()
        if not ret:
            break
        count += 1

    return count


def main():
    args = parse_args()

    # video capture
    video_capture = cv2.VideoCapture(args.video_path)
    if not video_capture.isOpened():
        print('failed to load video')
        exit(-1)

    w = round(video_capture.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = round(video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = video_capture.get(cv2.CAP_PROP_FPS)
    # num = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT))
    # if num < 0:
    #     num = count_video_frames(args.video_path)
    # print(w, h, fps, num)

    # video writer    
    video_name = os.path.splitext(os.path.basename(args.video_path))[0]
    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
    video_writer = cv2.VideoWriter(f'detection_{video_name}.avi', fourcc, fps, (w, h))
    
    # detector
    detector = Detector(
        model_path=args.model_path, device_name=args.device_name, device_id=0)
    
    # run
    while True:
        ret, frame = video_capture.read()
        if not(ret):
            break
    
        bboxes, labels, masks = detector(frame)

        indices = [i for i in range(len(bboxes))]
        for index, bbox, label_id in zip(indices, bboxes, labels):
            [left, top, right, bottom], score = bbox[0:4].astype(int), bbox[4]
            if score < 0.3:
                continue

            cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0))

            if masks[index].size:
                mask = masks[index]
                blue, green, red = cv2.split(frame)
                mask_img = blue[top:top + mask.shape[0], left:left + mask.shape[1]]
                cv2.bitwise_or(mask, mask_img, mask_img)
                frame = cv2.merge([blue, green, red])

        video_writer.write(frame)
        
    
    if video_capture.isOpened():
        video_capture.release()
        


if __name__ == '__main__':
    main()
