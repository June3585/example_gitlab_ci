# Copyright (c) OpenMMLab. All rights reserved.
import argparse
import os

import cv2


def parse_args():
    parser = argparse.ArgumentParser(
        description='show how to use sdk python api')
    parser.add_argument(
        'video_path', 
        help='name of device, cuda or cpu')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    video_capture = cv2.VideoCapture(args.video_path)
    if not video_capture.isOpened():
        print('failed to load video')
        exit(-1)

    w = round(video_capture.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = round(video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = video_capture.get(cv2.CAP_PROP_FPS)


    video_name = os.path.splitext(os.path.basename(args.video_path))[0]
    fourcc = cv2.VideoWriter_fourcc(*'h264')
    video_writer = cv2.VideoWriter(f'{video_name}_.mp4', fourcc, fps, (w, h))
    
    while True:
        ret, frame = video_capture.read()
        if not(ret):
            break
    video_writer.write(frame)
    

if __name__ == '__main__':
    main()
