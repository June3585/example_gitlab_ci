import argparse
import os
import cv2
import glob
parser  = argparse.ArgumentParser(description = 'Mapping labels public dataset to private dataset')

parser.add_argument('--path', '-p',
                    type=str,
                    required=True,
                    help='directory path of the target dataset')


if __name__ == '__main__':
    args = parser.parse_args()
    
    root_dir = args.path
    
    img_dir = os.path.join(root_dir, 'image')
    ann_dir = os.path.join(root_dir, 'det')
    layout_dir = os.path.join(root_dir, 'layout')

    os.makedirs(layout_dir, exist_ok=True)

    ann_list = os.listdir(ann_dir)
    ann_list.sort()

    root_dir_split = os.path.normpath(root_dir).split(os.sep)
    layout_path = os.path.join(layout_dir, root_dir_split[-1] + '.txt')
    with open(layout_path, 'w') as f:
        for i, ann_name in enumerate(ann_list):
            ann_basename = os.path.splitext(ann_name)[0]
            
            # TODO / FILTER / EXT
            # if not os.path.splitext(gt_file)[1] in ['.png', '.PNG']:
            #     continue
            
            # TODO / FILTER / MAX_NUM
            # if i > 3000:
            #     break;

            # TODO / ADD / IMAGE_INFO 
            # img_path = os.path.join(img_dir, ann_basename + '.jpg')
            # img = cv2.imread(img_path)
            # c, h, w = img
            # print(c, h, w)

            f.writelines(f"{ann_basename}\n")
            
