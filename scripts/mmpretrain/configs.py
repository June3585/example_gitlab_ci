'''
Usage:
(1) Use one of the following config files.
(2) Inside the config file, make sure that the dataset that needs to be trained on is uncommented.
(3) Use the appropriate input resolution in the config file (input_size).
(4) Recommend to run the first training with voc0712 dataset as it is widely used and reasonably small.
(5) To convert cityscapes to coco format, run the script: tools/convert_datasets/cityscapes.py

Example:
model_cfg='./configs/mmpretrain/mobilenet_v3/mobilenet_v3_small_posco_coupler.py'

deploy_cfg='./configs/mmdeploy/mmpretrain/classification_tensorrt_static-224x224.py'
'''
#
model_cfg='./configs/mmpretrain/mobilenet_v3/mobilenet_v3_small_posco_coupler.py'

#
deploy_cfg='./configs/mmdeploy/mmpretrain/classification_tensorrt_static-224x224.py'