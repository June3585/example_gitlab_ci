import sys
import os
import subprocess
from torch.distributed import launch as distributed_launch

########################################################################
# configs
from configs import model_cfg

########################################################################
# variables - distributed
distributed = 1

nnodes = 1
node_rank = 0
nproc_per_node = 2
master_addr = "127.0.0.1"
master_port = 29500

########################################################################
# variables - train
_, codebase, network, model = os.path.normpath(model_cfg).split(os.sep)
model_name = os.path.splitext(model)[0]

work_dir = os.path.join('./models', codebase, network, model_name)

########################################################################
# train
if distributed:
    sys.argv = [
        sys.argv[0], 
        f'--nnodes={nnodes}', 
        f'--node_rank={node_rank}', 
        f'--nproc_per_node={nproc_per_node}', 
        f'--master_addr={master_addr}', 
        f'--master_port={master_port}',
        '../openmmlab/codebase/mmpretrain/tools/train.py', 
        f'{model_cfg}',
        '--work-dir', f'{work_dir}',
        '--launcher=pytorch', 
    ]
    distributed_launch.main()
else:
    subprocess.run([
        'python',
        '../openmmlab/codebase/mmpretrain/tools/train.py', 
        f'{model_cfg}',
        '--work-dir', f'{work_dir}',
    ])

#