import sys
import os
import subprocess

########################################################################
# configs
from configs import model_cfg, deploy_cfg
# 
from mmengine.runner import find_latest_checkpoint

########################################################################
# variables
_, codebase, network, model = os.path.normpath(model_cfg).split(os.sep)
model_name = os.path.splitext(model)[0]

work_dir = os.path.join('./models', codebase, network, model_name)
model_pth = find_latest_checkpoint(work_dir)

######################################################################## 
# deploy
subprocess.run([
    'python',
    '../openmmlab/mmdeploy/tools/deploy.py', 
    f'{deploy_cfg}',
    f'{model_cfg}',
    f'{model_pth}',
    '../openmmlab/codebase/mmpretrain/demo/demo.JPEG',
    '--work-dir', f'{work_dir}',
    '--device', 'cuda',
    '--dump-info',
])

#