'''
Usage:
(1) Use one of the following config files.
(2) Inside the config file, make sure that the dataset that needs to be trained on is uncommented.
(3) Use the appropriate input resolution in the config file (input_size).
(4) Recommend to run the first training with voc0712 dataset as it is widely used and reasonably small.
(5) To convert cityscapes to coco format, run the script: tools/convert_datasets/cityscapes.py

Example:
model_cfg='./configs/mmdet/yolox/yolox_tiny_8x8_posco_object.py'
model_cfg='./configs/mmdet/yolox/yolox_tiny_8x8_posco_traffic.py'
model_cfg='./configs/mmdet/yolox/yolox_tiny_8x8_posco_railway.py'

model_cfg='./configs/mmdet/yolox/yolox_s_8x8_posco_object.py'
model_cfg='./configs/mmdet/yolox/yolox_s_8x8_posco_traffic.py'
model_cfg='./configs/mmdet/yolox/yolox_s_8x8_posco_railway.py'

deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt_static-416x416.py'
deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt_static-640x640.py'
deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt-fp32_dynamic-416x416.py'
deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt-fp32_dynamic-640x640.py'
deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt-int8_dynamic-416x416.py'
deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt-int8_dynamic-640x640.py'
'''
#
model_cfg='./configs/mmdet/yolox/yolox_s_8x8_posco.py'

#
deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt-fp32_dynamic-416x416.py'