import sys
import os
import subprocess
from torch.distributed import launch as distributed_launch

########################################################################
# config
from configs import model_cfg

########################################################################
# variables - distributed
distributed = 0

nnodes = 1
node_rank = 0
nproc_per_node = 2
master_addr = "127.0.0.1"
master_port = 29500

########################################################################
# variables - test
metric = 'mAP'  # 'bbox' or 'mAP'

_, codebase, network, model = os.path.normpath(model_cfg).split(os.sep)
model_name = os.path.splitext(model)[0]

work_dir = os.path.join('./models', codebase, network, model_name)

test_pkl = os.path.join(work_dir, 'result.pkl')
model_pth = os.path.join(work_dir, 'last_checkpoint')

######################################################################## 
# test

if distributed:
    sys.argv = [
        sys.argv[0], 
        f'--nnodes={nnodes}', 
        f'--node_rank={node_rank}', 
        f'--nproc_per_node={nproc_per_node}', 
        f'--master_addr={master_addr}', 
        f'--master_port={master_port}',
        '../openmmlab/codebase/mmdetection/tools/test.py', 
        f'{model_cfg}',
        f'{model_pth}',
        f'--eval={metric}',
        f'--out={test_pkl}',
        '--launcher=pytorch',
    ]
    distributed_launch.main()
else:
    subprocess.run([
        'python',
        '../openmmlab/codebase/mmdetection/tools/test.py', 
        f'{model_cfg}',
        f'{model_pth}',
        f'--eval={metric}',
        f'--out={test_pkl}',
    ])

# else:
    # from tools import train as train_mmdet
    # sys.argv = [sys.argv[0], f'--gpus={gpus}', '--no-validate',
    #             f'{config}']

    # args = train_mmdet.parse_args()
    # train_mmdet.main(args)
#