FROM nvcr.io/nvidia/pytorch:22.12-py3


# ─────────────────────────────────────────────────────────────────────────────
# WORKSPACE
# ─────────────────────────────────────────────────────────────────────────────
ENV ROOT=/root


# ─────────────────────────────────────────────────────────────────────────────
# OPENMMLAB CORE
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${ROOT}/3rdparty/openmmlab


# "mmengine"
RUN git clone -b main https://gitlab.com/June3585/mmengine.git mmengine \
    && cd mmengine \
    && pip install --no-cache-dir -e .

# "mmcv"
RUN git clone -b main https://gitlab.com/June3585/mmcv.git mmcv \
    && cd mmcv \
    && pip install --no-cache-dir -e .

# Verify the installation TODO: DELETED
RUN python -c 'from mmengine.utils.dl_utils import collect_env;print(collect_env())'


# ─────────────────────────────────────────────────────────────────────────────
# OPENMMLAB CODEBASE
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${ROOT}/3rdparty/openmmlab/codebase

# "mmpretrain"
RUN git clone -b main https://gitlab.com/June3585/mmpretrain.git mmpretrain \
    && cd mmpretrain \
    && pip install --no-cache-dir -e .

# "mmdetection"
RUN git clone -b main https://gitlab.com/June3585/mmdetection.git mmdetection \
    && cd mmdetection \
    && pip install --no-cache-dir -e .

# "mmrazor"
RUN git clone -b main https://gitlab.com/June3585/mmrazor.git mmrazor \
    && cd mmrazor \
    && pip install --no-cache-dir -e .


# ─────────────────────────────────────────────────────────────────────────────
# OPENMMLAB RUNTIME BACKEND
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${ROOT}/3rdparty/backend

# onnxruntime
# RUN wget https://github.com/microsoft/onnxruntime/releases/download/v1.8.1/onnxruntime-linux-x64-1.8.1.tgz \
RUN pip install onnxruntime-gpu==1.15.1
RUN wget https://github.com/microsoft/onnxruntime/releases/download/v1.15.1/onnxruntime-linux-x64-gpu-1.15.1.tgz \
    && tar -zxvf onnxruntime-linux-x64-gpu-1.15.1.tgz \
    && rm onnxruntime-linux-x64-gpu-1.15.1.tgz
ENV ONNXRUNTIME_DIR=${ROOT}/3rdparty/backend/onnxruntime-linux-x64-gpu-1.15.1
ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${ONNXRUNTIME_DIR}/lib

# "ppl.cv" # not suppurt cuda 12
RUN git clone https://github.com/openppl-public/ppl.cv.git ppl.cv \
    && cd ppl.cv \
    && ./build.sh cuda
ENV PPLCV_DIR=${ROOT}/3rdparty/backend/ppl.cv/cuda-build/install/lib/cmake/ppl

# "tensorrt"
ENV TENSORRT_DIR=/opt/tensorrt


# ─────────────────────────────────────────────────────────────────────────────
# OPENMMLAB RUNTIME SDK
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${ROOT}/3rdparty/openmmlab

# "mmdeploy"
RUN git clone --recursive -b main https://gitlab.com/June3585/mmdeploy.git mmdeploy \
    && cd mmdeploy \
    && pip install --no-cache-dir -e .

# "mmdeploy sdk" 
ENV BACKUP_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
ENV LD_LIBRARY_PATH=/usr/local/cuda/compat/lib.real/:$LD_LIBRARY_PATH
ENV MMDEPLOY_DIR=${ROOT}/3rdparty/openmmlab/mmdeploy
ENV MMDEPLOY_SDK_DIR=${MMDEPLOY_DIR}/build/install

RUN cd ${MMDEPLOY_DIR} \
    && mkdir -p build \
    && cd build \
    && cmake .. \
        -DCMAKE_CXX_COMPILER=g++ \
        -DMMDEPLOY_BUILD_SDK=ON \
        -DMMDEPLOY_BUILD_SDK_PYTHON_API=ON \
        -DMMDEPLOY_BUILD_EXAMPLES=ON \
        -DMMDEPLOY_TARGET_DEVICES="cuda;cpu" \
        -DMMDEPLOY_TARGET_BACKENDS="ort;trt" \
        -DCMAKE_INSTALL_PREFIX=${MMDEPLOY_SDK_DIR} \
        -DMMDEPLOY_CODEBASES="all" \
        -Dpplcv_DIR=${PPLCV_DIR} \
        -DONNXRUNTIME_DIR=${ONNXRUNTIME_DIR} \
        -DTENSORRT_DIR=${TENSORRT_DIR} \
    && make -j$(nproc) \
    && make install

ENV LD_LIBRARY_PATH=${MMDEPLOY_DIR}/build/lib:${BACKUP_LD_LIBRARY_PATH}
ENV LD_LIBRARY_PATH=${MMDEPLOY_DIR}/build/install/lib:${LD_LIBRARY_PATH}


# ─────────────────────────────────────────────────────────────────────────────
# WORKSPACE
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${ROOT}/workspace
